-- +----------------------------------------------------------------------------------+
-- | This program is free software; you can redistribute it and/or                    |
-- | modify it under the terms of the GNU General Public License                      |
-- | as published by the Free Software Foundation; either version 2                   |
-- | of the License, or (at your option) any later version.                           |
-- |                                                                                  |
-- | This program is distributed in the hope that it will be useful,                  |
-- | but WITHOUT ANY WARRANTY; without even the implied warranty of                   |
-- | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    |
-- | GNU General Public License for more details.                                     |
-- |                                                                                  |
-- | You should have received a copy of the GNU General Public License                |
-- | along with this program; if not, write to the Free Software                      |
-- | Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.  |
-- +----------------------------------------------------------------------------------+

__author__ = 'Boisei0'

EGGSIDE = 'back'
KILLSIDE = 'bottom'
FARMNAME = 'OCRSS Chicken Farm'

function cls()
  term.clear()
  term.setCursorPos(1,1)
end

function menu()
  cls()
  io.write('Welcome to the ')
  io.write(FARMNAME)
  io.write('\n')
  io.write('Please make a choise\n')
  io.write('[0] Here be dragons\n')
  io.write('[1] Add chickens\n')
  io.write('[2] Kill them all\n')
  choise = io.read()
  return choise
end

function main()
  cls()
  option = menu()
  if option == '0' then
    -- Wrong place?
    os.run({},'startup')
  elseif option == '1' then
    -- Add chickens
    io.write('How many chickens?\n')
    eggs = io.read() * 8 -- about one in every 8 eggs becomes a chicken
    index = 1
    while index < eggs do
      rs.setOutput(EGGSIDE, true)
      sleep(0.1)
      rs.setOutput(EGGSIDE, false)
      sleep(0.1)
      index = index + 1
    end
  elseif option == '2' then
    -- Kill them all!
    rs.setOutput(KILLSIDE, true)
    sleep(2)
    rs.setOutput(KILLSIDE, false)
    sleep(0.1)
  end
  main()
end

main()
