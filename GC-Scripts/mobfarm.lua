-- +---------------------+
-- |       LICENCE       |
-- +---------------------+

-- +----------------------------------------------------------------------------------+
-- | CC-Dump/FeedTheTrains Script to controll the redstone at our Soulshard mobfarms  |
-- | Copyright (C) 2013  Auke Bakker Aacoba@Aacoba.net                                |
-- +----------------------------------------------------------------------------------+
-- | This program is free software; you can redistribute it and/or                    |
-- | modify it under the terms of the GNU General Public License                      |
-- | as published by the Free Software Foundation; either version 2                   |
-- | of the License, or (at your option) any later version.                           |
-- |                                                                                  |
-- | This program is distributed in the hope that it will be useful,                  |
-- | but WITHOUT ANY WARRANTY; without even the implied warranty of                   |
-- | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    |
-- | GNU General Public License for more details.                                     |
-- |                                                                                  |
-- | You should have received a copy of the GNU General Public License                |
-- | along with this program; if not, write to the Free Software                      |
-- | Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.  |
-- +----------------------------------------------------------------------------------+

-- +--------------------+
-- |       CONFIG       |
-- +--------------------+

controlside = "right"

-- +--------------------+
-- |        TODO        |
-- +--------------------+
-- | Add normal mode    |
-- | Add gitget list    |
-- |                    |
-- |                    |
-- +--------------------+

-- +--------------------+
-- |        CODE        |
-- +--------------------+

function enable()
	if not status then
		rs.setOutput("bottom", true)
		rs.setOutput("top", false)
		status = true
	end
end

function disable()
	if status then
		rs.setOutput("top", true)
		sleep(10)
		rs.setOutput("bottom", false)
		status = false
	end
end


status = false
function main()
	if rs.getInput(controlside) then
		enable()
	else
		disable()
	end
end

rs.setOutput("top", true)
while true do
	main()
	sleep(0)
end