# FeedTheTrains Station Messageboards
  
  
## Instruction
1. Place a computer on the bottom row of the back of a 3 high and 5 wide screen and upload the master script to it
2. Place another computer on top of it and put the slave script on it
3. Put an howler alarm on the top of the slave computer, and connect lamps to the sides of the slave computer
4. Connect an redstone wire to the bottom of the master computer, toggle this to trigger the message
5. Enjoy ;)


## Licence
> This program is free software; you can redistribute it and/or
> modify it under the terms of the GNU General Public License
> as published by the Free Software Foundation; either version 2
> of the License, or (at your option) any later version.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
> 
> You should have received a copy of the GNU General Public License
> along with this program; if not, write to the Free Software
> Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
