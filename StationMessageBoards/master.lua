-- +---------------------+
-- |       LICENCE       |
-- +---------------------+

-- +----------------------------------------------------------------------------------+
-- | FeedTheTrains Station Messageboard master computer                               |
-- | Copyright (C) 2013  Auke Bakker Aacoba@Aacoba.net                                |
-- +----------------------------------------------------------------------------------+
-- | This program is free software; you can redistribute it and/or                    |
-- | modify it under the terms of the GNU General Public License                      |
-- | as published by the Free Software Foundation; either version 2                   |
-- | of the License, or (at your option) any later version.                           |
-- |                                                                                  |
-- | This program is distributed in the hope that it will be useful,                  |
-- | but WITHOUT ANY WARRANTY; without even the implied warranty of                   |
-- | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    |
-- | GNU General Public License for more details.                                     |
-- |                                                                                  |
-- | You should have received a copy of the GNU General Public License                |
-- | along with this program; if not, write to the Free Software                      |
-- | Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.  |
-- +----------------------------------------------------------------------------------+


-- +--------------------+
-- |       CONFIG       |
-- +--------------------+
stationName = " REPLACE  ME D:<"

-- +--------------------+
-- |        CODE        |
-- +--------------------+

mon = peripheral.wrap('back')


function start()
	rs.setOutput("top", true)
	TrainIncomming()
	sleep(10)
	slide1()
	slide2()
	rs.setOutput("top", false)
end

function Welkom()
	mon.clear()
	mon.setTextScale(3)
	mon.setCursorPos(1, 1)
	mon.setTextColor(colors.yellow)
	mon.write("   Bedankt voor")
	mon.setCursorPos(1, 2)
	mon.write("  het reizen met")
	mon.setCursorPos(1, 3)
	mon.setTextColor(colors.pink)
	mon.write("  FeedTheTrains")
	mon.setTextColor(colors.blue)
	mon.setCursorPos(1, 5)
	mon.write("    Station :")
	mon.setCursorPos(1, 6)
	mon.write(stationName)

end

function StationFile()
	if not fs.exists('stationConfig') then
		f = fs.open("stationConfig", 'w')
		f.write(" REPLACE  ME D:<")
		f.close()
		stationName = " REPLACE  ME D:<"
		print("No config found, creating..")
		print("Please edit..")
	else
		f = fs.open("stationConfig", 'r')
		stationName = f.readAll()
		print(stationName)
		f.close()
	end
	return stationName
end

function TrainIncomming()
	mon.clear()
	mon.setTextScale(3)
	mon.setCursorPos(1, 2)
	mon.setTextColor(colors.red)
	mon.write("     Pas  Op!")
	mon.setCursorPos(1, 4)
	mon.setTextColor(colors.yellow)
	mon.write("   Trein  komt")
	mon.setCursorPos(1, 5)
	mon.write(" Station binnen!")
end


function slide1()
	mon.clear()
	mon.setTextScale(4)
	mon.setCursorPos(1, 1)
	mon.setTextColor(colors.blue)
	mon.write("   Wil  je")
	mon.setCursorPos(1, 2)
	mon.write("   blijven")
	mon.setCursorPos(1, 4)
	mon.setTextColor(colors.red)
	mon.write("   LEVEN ?")
	x = 0
	while x < 4 do
		mon.clear()
		mon.setCursorPos(1, 1)
		mon.setTextColor(colors.blue)
		mon.write("   Wil  je")
		mon.setCursorPos(1, 2)
		mon.write("   blijven")
		mon.setCursorPos(1, 4)
		sleep(0.5)
		mon.setTextColor(colors.red)
		mon.write("   LEVEN ?")
		sleep(0.5)
		x = x + 1
	end
end

function slide2( ... )
	mon.clear()
	mon.setTextScale(5)
	mon.setCursorPos(1, 1)
	mon.setTextColor(colors.green)
	mon.setCursorPos(1, 2)
	mon.write("   WACHT DAN")
	mon.setCursorPos(1, 3)
	mon.write("     EVEN!!")
	x = 0
	while x < 4 do
		mon.clear()
		sleep(0.5)
			mon.setCursorPos(1, 2)
			mon.write("WACHT  DAN")
			mon.setCursorPos(1, 3)
			mon.write("  EVEN !!")
		sleep(0.5)
		x = x + 1
	end
end

stationName = StationFile()
while true do
	sleep(0)
	if rs.getInput("bottom") then
		start()
	else
		Welkom()
	end
end