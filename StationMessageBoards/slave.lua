-- +----------------------------------------------------------------------------------+
-- | FeedTheTrains Station Messageboard slave Computer                                |
-- | Copyright (C) 2013  Auke Bakker Aacoba@Aacoba.net                                |
-- +----------------------------------------------------------------------------------+
-- | This program is free software; you can redistribute it and/or                    |
-- | modify it under the terms of the GNU General Public License                      |
-- | as published by the Free Software Foundation; either version 2                   |
-- | of the License, or (at your option) any later version.                           |
-- |                                                                                  |
-- | This program is distributed in the hope that it will be useful,                  |
-- | but WITHOUT ANY WARRANTY; without even the implied warranty of                   |
-- | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    |
-- | GNU General Public License for more details.                                     |
-- |                                                                                  |
-- | You should have received a copy of the GNU General Public License                |
-- | along with this program; if not, write to the Free Software                      |
-- | Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.  |
-- +----------------------------------------------------------------------------------+


function flash()
	rs.setOutput("left", true)
	rs.setOutput("right", false)
	sleep(0.5)
	rs.setOutput("left", false)
	rs.setOutput("right", true)
	sleep(0.5)
end

while true do
	if rs.getInput("bottom") then
		rs.setOutput("top", true)
		flash()
	else
		rs.setOutput("top", false)
		rs.setOutput("left", false)
		rs.setOutput("right", false)
	end
	sleep(0)
end
